//
//  ViewController.m
//  AwesomeView
//
//  Created by meng yun on 2019/6/26.
//  Copyright © 2019 meng yun. All rights reserved.
//

#import "ViewController.h"
#import "AwesomeView/AwesomeView.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet AwesomeView *aView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//支持旋转
-(BOOL)shouldAutorotate{
    return true;
}

//支持的方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeRight;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationLandscapeRight;
}

- (IBAction)onClear:(id)sender {
    [_aView clean];
}

- (IBAction)onSetShowPoint:(id)sender {
    [_aView setShowPoint];
}

- (IBAction)onAdjust:(id)sender {
    [_aView adjust];
}


@end
