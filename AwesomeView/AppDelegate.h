//
//  AppDelegate.h
//  AwesomeView
//
//  Created by meng yun on 2019/6/26.
//  Copyright © 2019 meng yun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

