//
//  AwesomeView.m
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import "AwesomeView.h"
#import "AwesomePoint.h"
#import "AwesomeLine.h"
#import "AwesomeCircle.h"

@interface AwesomeView () <UIGestureRecognizerDelegate>
{
    BOOL _createDrawingPoints;
    BOOL _showPoints;
    CGFloat _accuracy; // 直线拟合的精度
    CGFloat _blendDepth; // 混合深度
    CGPoint _tmpPoint; //上次拖动的点
    
    // 点
    CGFloat _pointSize;
    UIColor *_pointColor;
    NSMutableSet<AwesomePoint *> *_points;
    
    // 线
    CGFloat _lineWidth;
    UIColor *_lineColor;
    NSString *_tmpKey;  // 每条完整的线条对于的碎线段
    NSMutableDictionary *_lines;
    
    // 圆
    UIColor *_fillColor;
    NSMutableSet<AwesomeCircle *> *_circles;
    NSMutableSet<AwesomeCircle *> *_tmpCircles;
}
@end

@implementation AwesomeView

- (id)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUp];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    
    return self;
}

- (void)setUp
{
    _showPoints = true;
    _createDrawingPoints = true;
    _accuracy = 0.55f;
    _blendDepth = 0.5f;
    
    _pointSize = 2.0f;
    _pointColor = [[UIColor redColor] colorWithAlphaComponent: 0.6];
    _points = [[NSMutableSet alloc] initWithCapacity: 5];
    
    _lineWidth = 1.0f;
    _lineColor = [[UIColor greenColor] colorWithAlphaComponent: 0.6];
    _lines = [[NSMutableDictionary alloc] init];
    
    _fillColor = [[UIColor greenColor] colorWithAlphaComponent: 0.1];
    _circles = [[NSMutableSet alloc] initWithCapacity: 3];
    _tmpCircles = [[NSMutableSet alloc] initWithCapacity: 4];
    
    _tmpPoint = CGPointZero;
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction:)];
    [panGesture setMaximumNumberOfTouches:1];
    panGesture.delegate = self;
    [self addGestureRecognizer:panGesture];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressAction:)];
    longPressGesture.delegate = self;
    [self addGestureRecognizer: longPressGesture];
}

// 画圆
- (void)longPressAction:(UILongPressGestureRecognizer *)longGesture
{
    CGPoint point = [longGesture locationInView:self];
    switch (longGesture.state) {
        case UIGestureRecognizerStateBegan:
            _tmpPoint = point;
        case UIGestureRecognizerStateChanged:{
            _tmpCircles = [[NSMutableSet alloc] initWithCapacity: 4];
            CGFloat r = sqrt((point.x-_tmpPoint.x)*(point.x-_tmpPoint.x) + (point.y-_tmpPoint.y)*(point.y-_tmpPoint.y));
            [_tmpCircles addObject: [[AwesomeCircle alloc] initWithCenter:_tmpPoint radius: r]];
            [self setNeedsDisplay];
        }
            break;
        case UIGestureRecognizerStateEnded:{
            _tmpCircles = [[NSMutableSet alloc] initWithCapacity: 4];
            CGFloat r = sqrt((point.x-_tmpPoint.x)*(point.x-_tmpPoint.x) + (point.y-_tmpPoint.y)*(point.y-_tmpPoint.y));
            [_circles addObject: [[AwesomeCircle alloc] initWithCenter:_tmpPoint radius: r]];
            [self setNeedsDisplay];
        }
            break;
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateCancelled: {
        }
            break;
            
        default:
            break;
    }
    
}

// 画线
- (void)panAction:(UIPanGestureRecognizer *)panGesture
{
    NSLog(@"-------- %s  %lf", __func__, _tmpPoint.x);
    CGPoint point = [panGesture locationInView: self];
    
    switch (panGesture.state) {
        case UIGestureRecognizerStateBegan: {
            if (_createDrawingPoints)
                [_points addObject: [[AwesomePoint alloc] initWithPoint: point]];
            _tmpPoint = point;
            CGFloat time = [[NSDate date] timeIntervalSince1970];
            _tmpKey = [NSString stringWithFormat: @"%@_%lf", NSStringFromCGPoint(_tmpPoint), time];
            _lines[_tmpKey] = [[NSMutableArray alloc] init];
        } break;
            
        case UIGestureRecognizerStateChanged: {
            if (_createDrawingPoints)
                [_points addObject: [[AwesomePoint alloc] initWithPoint: point]];
            [_lines[_tmpKey] addObject: [[AwesomeLine alloc] initWithStart:_tmpPoint end: point]];
            _tmpPoint = point;
        }
            break;
            
        case UIGestureRecognizerStateEnded:{
            if (_createDrawingPoints)
                [_points addObject: [[AwesomePoint alloc] initWithPoint: point]];
        } break;
        case UIGestureRecognizerStateCancelled:

            break;
            
        default:
            break;
    }
    
    [self setNeedsDisplay];
}

- (void) injected{
    NSLog(@"I've been injected   : %@", self);
    NSLog(@"::: %lf", atanf(11111));
    //[self setUp];
    _createDrawingPoints = false;
    // [self adjustLines];
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [self bezierDraw];
}

- (void) bezierDraw{
    NSLog(@"bezierDraw_");
    [_lineColor setStroke];
    [_pointColor setFill];
    // 画线
    for(NSString *key in _lines)
    {
        // NSLog(@"bezierDraw_key:%@ value:%@",key,_lines[key]);
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        bezierPath.lineWidth = _lineWidth;
        NSArray *curLines = _lines[key];
        for (int i=0; i<curLines.count; ++i){
            AwesomeLine *l = curLines[i];
            CGPoint start = l.start;
            CGPoint end = l.end;
            if (i==0){
                [bezierPath moveToPoint: start];
                [bezierPath addLineToPoint: end];
            }
            else{
                [bezierPath addLineToPoint: start];
                if (i==curLines.count-1){
                    [bezierPath addLineToPoint: end];
                    // [bezierPath closePath];
                    l.needDraw = false;
                }
            }
        }
        [bezierPath strokeWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
    }
    
    // 画点
    [_pointColor setFill];
    if (_showPoints){
        for (AwesomePoint *p in _points){
            UIBezierPath *bezierPath = [UIBezierPath bezierPath];
            [bezierPath addArcWithCenter: p.p radius: _pointSize*0.5 startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
            [bezierPath fill];
        }
    }
    
    // 画正在拖拽中的圆
    for (AwesomeCircle *c in _tmpCircles){
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        bezierPath.lineWidth = _lineWidth;
        [bezierPath addArcWithCenter: c.center radius: _pointSize*0.5 startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
        [_pointColor setFill];
        [bezierPath fill];
        
        [bezierPath removeAllPoints];
        CGFloat dashLineConfig[] = {8.0, 4.0};
        [bezierPath setLineDash: dashLineConfig count: 2 phase: 0];
        CGFloat rr = c.radius;
        [bezierPath addArcWithCenter: c.center radius: rr startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
        [bezierPath strokeWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
        [_fillColor setFill];
        [bezierPath fillWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
    }
    // 画圆
    for (AwesomeCircle *c in _circles){
        UIBezierPath *bezierPath = [UIBezierPath bezierPath];
        bezierPath.lineWidth = _lineWidth;
        [bezierPath addArcWithCenter: c.center radius: _pointSize*0.5 startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
        [_pointColor setFill];
        [bezierPath fill];
        
        [bezierPath removeAllPoints];
        CGFloat rr = c.radius;
        [bezierPath addArcWithCenter: c.center radius: rr startAngle: 0.0 endAngle:M_PI * 2 clockwise: true];
        [bezierPath strokeWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
        [_fillColor setFill];
        [bezierPath fillWithBlendMode: kCGBlendModeMultiply alpha: _blendDepth];
    }
}

// 线条调整成直线
- (void) adjustLines{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    for(NSString *key in _lines)
    {
        NSArray *curLines = _lines[key];
        dic[key] = [self adjustLineWithLines: curLines];
        NSLog(@"----: %ld %ld", curLines.count, ((NSArray *)dic[key]).count);
    }
    _lines = dic;
    // _points = [[NSMutableSet alloc] initWithCapacity: 5];
}
- (NSArray *) adjustLineWithLines: (NSArray *)lines{
    NSLog(@"----: calladjustLineWithLines");
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    CGPoint tmpStart = CGPointZero;
    CGFloat tmpSlope = -990;
    for (int i=0; i<lines.count; ++i){
        AwesomeLine *l = lines[i];
        CGPoint start = l.start;
        CGPoint end = l.end;
        if (i==0){
            tmpStart = start;
            tmpSlope = [self slopeWithStart: start end: end];
            if (i==lines.count-1){
                NSLog(@"----: slopeadd 111111");
                [arr addObject: [[AwesomeLine alloc] initWithStart: tmpStart end: end]];
            }
        }
//        else if (i==lines.count-1){
//            NSLog(@"----: slopeadd 2222222 %ld %ld | %lf %lf", i, lines.count, l.start.x, l.start.y);
//            [arr addObject: [[AwesomeLine alloc] initWithStart:tmpStart end: end]];
//        }
         else{
            CGFloat curSlope = [self slopeWithStart: start end: end];
             NSLog(@"----: slope: %ld | %lf %lf | %lf %lf",arr.count ,curSlope, tmpSlope, (curSlope-tmpSlope)*(curSlope-tmpSlope),_accuracy*_accuracy);
            if ((curSlope-tmpSlope)*(curSlope-tmpSlope)<_accuracy*_accuracy){ // 与上一个线段在一条直线上
                tmpSlope = [self slopeWithStart: tmpStart end: end];
                // tmpStart = end;
            }
            else{   // 与上一个线段不在一条直线上, 开始一个新的线段
                NSLog(@"----: slopeadd 333333333");
                [arr addObject: [[AwesomeLine alloc] initWithStart:tmpStart end: start]];
                // tmpSlope = (start.y-tmpStart.y)/(start.x-tmpStart.x);
                tmpSlope = [self slopeWithStart: start end: end];
                tmpStart = start;
            }
             
            if (i==lines.count-1){
                NSLog(@"----: slopeadd 2222222 %ld %ld | %lf %lf", i, lines.count, l.start.x, l.start.y);
                [arr addObject: [[AwesomeLine alloc] initWithStart:tmpStart end: end]];
            }
         }
    }
    return [arr copy];
}

- (CGFloat) slopeWithStart: (CGPoint) start end: (CGPoint) end{
    CGFloat deltaX = end.x-start.x;
    CGFloat deltaY = end.y-start.y;
    
    if (deltaX == 0)
        return atanf(9999);
    return atanf(deltaY/deltaX);
}

- (void) clean{
    [self setUp];
    [self setNeedsDisplay];
}

- (void) adjust{
    [self adjustLines];
    [self setNeedsDisplay];
}

- (void)setShowPoint{
    _showPoints = !_showPoints;
    [self setNeedsDisplay];
}

@end
