//
//  AwesomeLine.m
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import "AwesomeLine.h"

@implementation AwesomeLine

- (AwesomeLine *)initWithStart: (CGPoint)start end: (CGPoint)end{
    AwesomeLine *l = [[AwesomeLine alloc] init];
    l.start = start;
    l.end = end;
    l.needDraw = true;
    return l;
}

@end
