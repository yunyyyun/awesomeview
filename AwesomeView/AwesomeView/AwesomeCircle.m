//
//  AwesomeCircle.m
//  PlotKitTest
//
//  Created by meng yun on 2019/6/25.
//  Copyright © 2019 mengyun. All rights reserved.
//

#import "AwesomeCircle.h"

@implementation AwesomeCircle

- (AwesomeCircle *)initWithCenter: (CGPoint)center radius: (CGFloat)radius{
    AwesomeCircle *circle = [[AwesomeCircle alloc] init];
    circle.center = center;
    circle.radius = radius;
    circle.needDraw = true;
    
    return circle;
}

@end
